#!/usr/bin/env python

import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BOARD)

GREEN = 33 # Pin 12 Latch clock
YELLOW = 35 # Pin 11 shift clock
RED = 37 # Pin 14 A

GPIO.setup(GREEN, GPIO.OUT) # P0 
GPIO.setup(YELLOW, GPIO.OUT) # P1 
GPIO.setup(RED, GPIO.OUT) # P7 

# Setup IO
GPIO.output(GREEN, 0)
GPIO.output(YELLOW, 0)
GPIO.output(RED, 0)

try:
	while 1:
		GPIO.output(GREEN, 1)
		GPIO.output(YELLOW, 0)
		GPIO.output(RED, 0)
		time.sleep(2)
		GPIO.output(GREEN, 0)
		GPIO.output(YELLOW, 1)
		GPIO.output(RED, 0)
		time.sleep(2)
		GPIO.output(GREEN, 0)
		GPIO.output(YELLOW, 0)
		GPIO.output(RED, 1)
		time.sleep(2)

except KeyboardInterrupt:
	print "Good bye!"
finally:
	GPIO.cleanup()