#!/usr/bin/env python

import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BOARD)

latchPin = 33
clockPin = 35
dataPin = 37

semaforo1 = 0
semaforo2 = 0

GPIO.setup(latchPin, GPIO.OUT)
GPIO.setup(clockPin, GPIO.OUT)
GPIO.setup(dataPin, GPIO.OUT)

def shiftOut(myDataPin, myClockPin, myDataOut):
	i = 0
	GPIO.output(myDataPin, 0)
	GPIO.output(myClockPin, 0)
	for i in range(8,0,-1):
		GPIO.output(myClockPin, 0)
		if (myDataOut & (1<<i)):
			pinState = 1
		else:
			pinState = 0

		GPIO.output(myDataPin, pinState)
		GPIO.output(myClockPin, 1)
		GPIO.output(myDataPin, 0)
	GPIO.output(myClockPin, 1)
	return

try:
	state = 1
	
	while 1:
		if state == 1:
			print "S1\n10"
			data = 0x42
			#Semaforo 1: Verde
			semaforo1 = 1
			#Semaforo 2: Rojo
			semaforo2 = 0
			file = open("/home/pi/smartcity/html/lightState.txt", "w")
			file.write("%d" % semaforo1)
			file.write("%d" % semaforo2)
			file.close()
			GPIO.output(latchPin, 0)
			shiftOut(dataPin, clockPin, data)
			GPIO.output(latchPin, 1)
			time.sleep(10)
			state = 2

		elif state == 2:
			print "S2\n10"
			data = 0x40
			#Semaforo 1: Verde parpadeo
			#semaforo1 = 1
			#Semaforo 2: Rojo
			#semaforo2 = 0
			#file = open("/home/pi/smartcity/html/lightState.txt", "w")
			#file.write("%d" % semaforo1)
			#file.write("%d" % semaforo2)
			#file.close()
			GPIO.output(latchPin, 0)
			shiftOut(dataPin, clockPin, data)
			GPIO.output(latchPin, 1)
			time.sleep(.5)

			data = 0x42
			GPIO.output(latchPin, 0)
			shiftOut(dataPin, clockPin, data)
			GPIO.output(latchPin, 1)
			time.sleep(.5)

			data = 0x40
			GPIO.output(latchPin, 0)
			shiftOut(dataPin, clockPin, data)
			GPIO.output(latchPin, 1)
			time.sleep(.5)

			data = 0x42
			GPIO.output(latchPin, 0)
			shiftOut(dataPin, clockPin, data)
			GPIO.output(latchPin, 1)
			time.sleep(.5)

			data = 0x40
			GPIO.output(latchPin, 0)
			shiftOut(dataPin, clockPin, data)
			GPIO.output(latchPin, 1)
			time.sleep(.5)

			state = 3

		elif state == 3:
			print "S3\n00"
			data = 0x44
			#Semaforo 1: Amarillo
			semaforo1 = 0
			#Semaforo 2: Rojo
			semaforo2 = 0
			file = open("/home/pi/smartcity/html/lightState.txt", "w")
			file.write("%d" % semaforo1)
			file.write("%d" % semaforo2)
			file.close()
			GPIO.output(latchPin, 0)
			shiftOut(dataPin, clockPin, data)
			GPIO.output(latchPin, 1)
			time.sleep(2)
			state = 4

		elif state == 4:
			print "S4\n01"
			data = 0x18
			#Semaforo 1: Rojo
			semaforo1 = 0
			#Semaforo 2: Verde
			semaforo2 = 1
			file = open("/home/pi/smartcity/html/lightState.txt", "w")
			file.write("%d" % semaforo1)
			file.write("%d" % semaforo2)
			file.close()
			GPIO.output(latchPin, 0)
			shiftOut(dataPin, clockPin, data)
			GPIO.output(latchPin, 1)
			time.sleep(10)
			state = 5

		elif state == 5:
			print "S5\n01"
			data = 0x08
			#Semaforo 1: Rojo
			#semaforo1 = 0
			#Semaforo 2: Verde parpadea
			#semaforo2 = 1
			#file = open("/home/pi/smartcity/html/lightState.txt", "w")
			#file.write("%d" % semaforo1)
			#file.write("%d" % semaforo2)
			#file.close()
			GPIO.output(latchPin, 0)
			shiftOut(dataPin, clockPin, data)
			GPIO.output(latchPin, 1)
			time.sleep(.5)

			data = 0x18
			GPIO.output(latchPin, 0)
			shiftOut(dataPin, clockPin, data)
			GPIO.output(latchPin, 1)
			time.sleep(.5)

			data = 0x08
			GPIO.output(latchPin, 0)
			shiftOut(dataPin, clockPin, data)
			GPIO.output(latchPin, 1)
			time.sleep(.5)

			data = 0x18
			GPIO.output(latchPin, 0)
			shiftOut(dataPin, clockPin, data)
			GPIO.output(latchPin, 1)
			time.sleep(.5)

			data = 0x08
			GPIO.output(latchPin, 0)
			shiftOut(dataPin, clockPin, data)
			GPIO.output(latchPin, 1)
			time.sleep(.5)

			state = 6

		elif state == 6:
			print "S6\n00"
			data = 0x28
			#Semaforo 1: Rojo
			semaforo1 = 0
			#Semaforo 2: Amarillo
			semaforo2 = 0
			file = open("/home/pi/smartcity/html/lightState.txt", "w")
			file.write("%d" % semaforo1)
			file.write("%d" % semaforo2)
			file.close()
			GPIO.output(latchPin, 0)
			shiftOut(dataPin, clockPin, data)
			GPIO.output(latchPin, 1)
			time.sleep(2)
			state = 1
except KeyboardInterrupt:
	print "Good bye!"
finally:
	GPIO.cleanup()
