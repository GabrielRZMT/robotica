#!/usr/bin/env python

import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BOARD)

latchPin = 33
clockPin = 35
dataPin = 37

GPIO.setup(latchPin, GPIO.OUT)
GPIO.setup(clockPin, GPIO.OUT)
GPIO.setup(dataPin, GPIO.OUT)

def shiftOut(myDataPin, myClockPin, myDataOut):
	i = 0
	GPIO.output(myDataPin, 0)
	GPIO.output(myClockPin, 0)
	for i in range(7,0,-1):
		GPIO.output(myClockPin, 0)
		if (myDataOut & (1<<i)):
			pinState = 1
		else:
			pinState = 0

		GPIO.output(myDataPin, pinState)
		GPIO.output(myClockPin, 1)
		GPIO.output(myDataPin, 0)
	GPIO.output(myClockPin, 1)
	return

try:
	state = 1

	while 1:
		if state == 1:
			print "S1"
			data = 0x21
			GPIO.output(latchPin, 0)
			shiftOut(dataPin, clockPin, data)
			GPIO.output(latchPin, 1)
			time.sleep(10)
			state = 2

		elif state == 2:
			print "S2"
			data = 0x20
			GPIO.output(latchPin, 0)
			shiftOut(dataPin, clockPin, data)
			GPIO.output(latchPin, 1)
			time.sleep(.5)

			data = 0x21
			GPIO.output(latchPin, 0)
			shiftOut(dataPin, clockPin, data)
			GPIO.output(latchPin, 1)
			time.sleep(.5)

			data = 0x20
			GPIO.output(latchPin, 0)
			shiftOut(dataPin, clockPin, data)
			GPIO.output(latchPin, 1)
			time.sleep(.5)

			data = 0x21
			GPIO.output(latchPin, 0)
			shiftOut(dataPin, clockPin, data)
			GPIO.output(latchPin, 1)
			time.sleep(.5)

			data = 0x20
			GPIO.output(latchPin, 0)
			shiftOut(dataPin, clockPin, data)
			GPIO.output(latchPin, 1)
			time.sleep(.5)

			state = 3

		elif state == 3:
			print "S3"
			data = 0x22
			GPIO.output(latchPin, 0)
			shiftOut(dataPin, clockPin, data)
			GPIO.output(latchPin, 1)
			time.sleep(2)
			state = 4

		elif state == 4:
			print "S4"
			data = 0x0C
			GPIO.output(latchPin, 0)
			shiftOut(dataPin, clockPin, data)
			GPIO.output(latchPin, 1)
			time.sleep(10)
			state = 5

		elif state == 5:
			print "S5"
			data = 0x04
			GPIO.output(latchPin, 0)
			shiftOut(dataPin, clockPin, data)
			GPIO.output(latchPin, 1)
			time.sleep(.5)

			data = 0x0C
			GPIO.output(latchPin, 0)
			shiftOut(dataPin, clockPin, data)
			GPIO.output(latchPin, 1)
			time.sleep(.5)

			data = 0x04
			GPIO.output(latchPin, 0)
			shiftOut(dataPin, clockPin, data)
			GPIO.output(latchPin, 1)
			time.sleep(.5)

			data = 0x0C
			GPIO.output(latchPin, 0)
			shiftOut(dataPin, clockPin, data)
			GPIO.output(latchPin, 1)
			time.sleep(.5)

			data = 0x04
			GPIO.output(latchPin, 0)
			shiftOut(dataPin, clockPin, data)
			GPIO.output(latchPin, 1)
			time.sleep(.5)

			state = 6

		elif state == 6:
			print "S6"
			data = 0x14
			GPIO.output(latchPin, 0)
			shiftOut(dataPin, clockPin, data)
			GPIO.output(latchPin, 1)
			time.sleep(2)
			state = 1
		
except KeyboardInterrupt:
	print "Good bye!"
finally:
	GPIO.cleanup()
