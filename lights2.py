#!/usr/bin/env python

import RPi.GPIO as GPIO
import time

GPIO.cleanup()
GPIO.setmode(GPIO.BOARD)

LATCH = 33
CLK = 35
dataBit = 37

GPIO.setup(CLK, GPIO.OUT)
GPIO.setup(dataBit, GPIO.OUT)
GPIO.setup(LATCH,GPIO.OUT)

state = 1

def shiftOut(myDataPin, myClockPin, myDataOut):
	i = 0
	
	GPIO.output(myClockPin, 0)
	GPIO.output(myDataPin, 0)

	for i in range(7,0,-1):
		GPIO.output(myClockPin, 0)
		if myDataOut & (1<<i):
			pinState = 1
		else:
			pinState = 0

		GPIO.output(myDataPin, pinState)
		GPIO.output(myClockPin, 1)
		GPIO.output(myDataPin, 0)
	GPIO.output(myClockPin, 0)
		
while 1:
	if state == 1:
		print "S 1"
		data = 0x21
		GPIO.output(LATCH, 0)
		shiftOut(LATCH, CLK, dataBit)
		GPIO.output(LATCH, 1)
		time.sleep(5)
		state = 2
	elif state == 2:
		print "S2" 
		data = 0x20
                GPIO.output(LATCH, 0)
                shiftOut(LATCH, CLK, dataBit)
                GPIO.output(LATCH, 1)
                time.sleep(.5)
                
		data = 0x21
                GPIO.output(LATCH, 0)
                shiftOut(LATCH, CLK, dataBit)
                GPIO.output(LATCH, 1)
                time.sleep(.5)

		data = 0x20
                GPIO.output(LATCH, 0)
                shiftOut(LATCH, CLK, dataBit)
                GPIO.output(LATCH, 1)
                time.sleep(.5)
	
		data = 0x21
                GPIO.output(LATCH, 0)
                shiftOut(LATCH, CLK, dataBit)
                GPIO.output(LATCH, 1)
                time.sleep(.5)

		data = 0x20
                GPIO.output(LATCH, 0)
                shiftOut(LATCH, CLK, dataBit)
                GPIO.output(LATCH, 1)
                time.sleep(.5)

		state = 3
	
	elif state == 3:
		print "S3"
                data = 0x22
                GPIO.output(LATCH, 0)
                shiftOut(LATCH, CLK, dataBit)
                GPIO.output(LATCH, 1)
                time.sleep(2)
                state = 4

	elif state == 4:
		print "S 4"
                data = 0x0C
                GPIO.output(LATCH, 0)
                shiftOut(LATCH, CLK, dataBit)
                GPIO.output(LATCH, 1)
                time.sleep(5)
                state = 1
